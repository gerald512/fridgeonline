﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Repository.DAL;
using Repository.Models;
using Repository.Interfaces;
using Microsoft.AspNet.Identity;
using PagedList;

namespace FridgeOnline.Controllers
{
    [Authorize]
    public class FridgeController : Controller
    {
        private readonly IFridge repo;

        public FridgeController(IFridge repo)
        {
            this.repo = repo;
        }

        // GET: Fridge
        public ActionResult Index(string ProductsSought, int? page)
        {
            IEnumerable<Product> foundProducts;
            string userID = User.Identity.GetUserId();
            int currentPage = page ?? 1;
            int onPage = 5;            

            if (ProductsSought == "")
                ProductsSought = null;

            if (ProductsSought != null)
            {
                foundProducts = repo.GetProducts().Where(s => s.Type == ProductsSought);

                if (foundProducts.Count() == 0)
                {
                    ViewBag.Error = true;
                    return View(foundProducts.OrderBy(o => o.ExpirationDate).ToPagedList<Product>(currentPage, onPage));
                }
            }
            else
                foundProducts = repo.GetProducts().Where(model => model.UserId == userID && model.InShoppingList == false);            

            if (Request.IsAjaxRequest())
                return View(foundProducts.OrderBy(o => o.ExpirationDate).ToPagedList<Product>(currentPage, onPage));

            return View(foundProducts.OrderBy(o => o.ExpirationDate).ToPagedList<Product>(currentPage, onPage));
        }

        // GET: Fridge/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Fridge/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Type,Count,TypeOfCountList,ExpirationDate")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.UserId = User.Identity.GetUserId();

                try
                {
                    repo.AddProduct(product);
                    repo.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(product);
                }
            }

            return View(product);
        }

        // POST: Fridge/Delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            repo.DeleteProduct(id);

            try
            {
                repo.SaveChanges();
            }
            catch
            {
                return View("Index");
            }
                        
            return RedirectToAction("Index");
        }

        public ActionResult ChangeCountOfProduct(int id)
        {
            var product = repo.FindProductById(id);

            if (product == null)
                return HttpNotFound();

            return View(product);
        }

        [HttpPost]
        public ActionResult ChangeCountOfProduct(Product product)
        {
            repo.DecreaseProductCount(product);

            if (product.Count < 0)
            {
                ViewBag.Error = true;
                return View(product);
            }

            else
            {
                try
                {
                    repo.SaveChanges();
                }
                catch
                {
                    ViewBag.Error = true;
                    return View("Index");
                }
            }

            ViewBag.Error = false;
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Product product = repo.FindProductById(id);

            if (product == null)
                return HttpNotFound();

            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            try
            {
                repo.Edit(product);
                repo.SaveChanges();
            }
            catch
            {
                ViewBag.Error = true;
                return View("Index");
            }


            ViewBag.Error = false;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult MoveToShoppingList(int id)
        {
            repo.MoveToShoppingList(id);

            try
            {
                repo.SaveChanges();
            }
            catch
            {
                return View("Index");
            }

            return RedirectToAction("Index", "ShoppingList");
        }
       
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
