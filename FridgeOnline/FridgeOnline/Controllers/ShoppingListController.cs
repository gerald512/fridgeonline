﻿using Microsoft.AspNet.Identity;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FridgeOnline.Controllers
{
    [Authorize]
    public class ShoppingListController : Controller
    {
        private readonly IFridge repo;
        private readonly IShoppingList shoppingList;

        public ShoppingListController(IFridge repo, IShoppingList shoppingList)
        {
            this.repo = repo;
            this.shoppingList = shoppingList;
        }

        // GET: ShoppingList
        public ActionResult Index()
        {
            var products = repo.GetProducts();
            string userID = User.Identity.GetUserId();
            products = products.Where(model => model.UserId == userID && model.InShoppingList == true);
            return View(products);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            repo.DeleteProduct(id);

            try
            {
                repo.SaveChanges();
            }
            catch
            {
                return View("Index");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Type,Count,TypeOfCountList")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.UserId = User.Identity.GetUserId();
                product.ExpirationDate = null;
                product.InShoppingList = true;

                try
                {
                    repo.AddProduct(product);
                    repo.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(product);
                }
            }

            return View(product);
        }

        [HttpPost]
        public ActionResult MoveProductsToFridge()
        {
            try
            {
                shoppingList.MoveToFridge();
                repo.SaveChanges();
                return RedirectToAction("Index", "Fridge");
            }
            catch
            {
                return View("Index");
            }
        }

        public ActionResult Edit(int id)
        {
            Product product = repo.FindProductById(id);

            if (product == null)
                return HttpNotFound();

            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            try
            {
                shoppingList.EditProductInShoppingList(product);
                repo.SaveChanges();
            }
            catch
            {
                ViewBag.Error = true;
                return View("Index");
            }

            ViewBag.Error = false;
            return RedirectToAction("Index");
        }
    }
}