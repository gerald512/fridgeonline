﻿using Microsoft.AspNet.Identity;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FridgeOnline.Controllers
{
    public class UserController : Controller
    {
        private readonly IAccountOperations Operations;

        public UserController(IAccountOperations Operations)
        {
            this.Operations = Operations;
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                Operations.DeleteConnectionProducts(id);
                Operations.DeleteUser(id);
                Operations.SaveChanges();
                Request.GetOwinContext().Authentication.SignOut();
            }
            catch
            {
                return View("Index", "Home");
            }
            
            return RedirectToAction("Index", "Home");
        }
    }
}