﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FridgeOnline.Startup))]
namespace FridgeOnline
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
