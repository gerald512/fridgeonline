﻿using Microsoft.AspNet.Identity.EntityFramework;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Repository.DAL
{
    public class ProductsContext : IdentityDbContext, IProductsContext
    {
        public ProductsContext() : base("ProductsContext") { }

        public static ProductsContext Create()
        {
            return new ProductsContext();
        }

        public DbSet<Product> Product { get; set; }
        public DbSet<User> User { get; set; }
    }
}