﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IAccountOperations
    {
        User FindUser(string id);
        void DeleteUser(string id);
        void DeleteConnectionProducts(string userId);
        void SaveChanges();
    }
}
