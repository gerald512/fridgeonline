﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IFridge
    {
        IQueryable<Product> GetProducts();
        void DeleteProduct(int id);
        Product FindProductById(int id);
        void SaveChanges();
        void AddProduct(Product product);
        List<Product> FindProductByTypeAndUser(Product product);
        void DecreaseProductCount(Product product);
        void Edit(Product product);
        void MoveToShoppingList(int id);
    }
}
