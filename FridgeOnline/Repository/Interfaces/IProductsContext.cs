﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IProductsContext
    {
        DbSet<Product> Product { get; set; }
        DbSet<User> User { get; set; }
        int SaveChanges();
        DbEntityEntry Entry(object entity);
    }
}
