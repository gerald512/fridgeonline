﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Repository.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        [NotMapped]
        [Display(Name = "Take")]
        public double HowManyDecreaseCount { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        public string Type { get; set; }

        [Range(0, 2000, ErrorMessage = "This number is out of range!!! Range: 0 - 2000")]
        [Required(ErrorMessage = "This field is required!")]
        public double Count { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        public TypeOfCount TypeOfCountList { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Expiration date")]
        public DateTime? ExpirationDate { get; set; }

        public bool InShoppingList { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }

    public enum TypeOfCount
    {
        kg,
        dek,
        g,
        ml,
        l,
        szt
    }
}