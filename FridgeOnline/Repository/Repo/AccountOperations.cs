﻿using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Repository.Repo
{
    public class AccountOperations : IAccountOperations
    {
        private readonly IProductsContext db;

        public AccountOperations(IProductsContext db)
        {
            this.db = db;
        }

        public User FindUser(string id)
        {
            var user = db.User.Where(m => m.UserName == id);
            var us = user.First();
            return us;
        }

        public void DeleteUser(string id)
        {
            var user = FindUser(id);
            db.User.Remove(user);
        }

        public void DeleteConnectionProducts(string userId)
        {
            var list = db.Product.Where(m => m.User.UserName == userId);
            foreach (var item in list)
            {
                db.Product.Remove(item);
            }
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}