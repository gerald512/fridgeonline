﻿using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Repository.Repo
{
    public class Fridge : IFridge
    {
        private readonly IProductsContext db;

        public Fridge(IProductsContext db)
        {
            this.db = db;
        }

        public IQueryable<Product> GetProducts()
        {
            var products = db.Product;
            return products;
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        public void AddProduct(Product product)
        {
            var foundProducts = FindProductByTypeAndUser(product);

            if (product.InShoppingList == true)
                db.Product.Add(product);
            else if (foundProducts.Any(o => o.ExpirationDate == product.ExpirationDate))
            {
                foreach (var item in foundProducts)
                {
                    if (product.ExpirationDate == item.ExpirationDate)
                    {
                        item.Count += product.Count;
                        break;
                    }
                }
            }                   
            else
                db.Product.Add(product);
        }

        public void DeleteProduct(int id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
        }

        public List<Product> FindProductByTypeAndUser(Product product)
        {
            var products = GetProducts();
            var foundProducts = products.Where(o => o.Type == product.Type && o.UserId == product.UserId).ToList();
            return foundProducts;
        }

        public Product FindProductById(int id)
        {
            Product product = db.Product.Find(id);
            return product;
        }

        public void DecreaseProductCount(Product product)
        {
            product.Count -= product.HowManyDecreaseCount;
            db.Entry(product).State = EntityState.Modified;
        }

        public void Edit(Product product)
        {
            var foundProducts = FindProductByTypeAndUser(product);

            if (foundProducts.Any(o => o.ExpirationDate == product.ExpirationDate))
            {
                foreach (var item in foundProducts)
                {
                    if (product.ExpirationDate == item.ExpirationDate)
                    {
                        item.Count += product.Count;
                        DeleteProduct(product.ProductId);
                        break;
                    }
                }
            }
            else
            {
                var productToEdit = db.Product.First(p => p.ProductId == product.ProductId);
                productToEdit.ExpirationDate = product.ExpirationDate;
            }                           
        }

        public void MoveToShoppingList(int id)
        {
            Product product = db.Product.Find(id);
            product.InShoppingList = true;
            product.ExpirationDate = null;
        }
    }
}