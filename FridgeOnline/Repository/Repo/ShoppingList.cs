﻿using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Repository.Repo
{
    public class ShoppingList : IShoppingList
    {
        private readonly IProductsContext db;

        public ShoppingList(IProductsContext db)
        {
            this.db = db;
        }

        public void MoveToFridge()
        {
            var productsInShoppingList = db.Product.Where(p => p.InShoppingList == true);
            foreach (var item in productsInShoppingList)
            {
                item.InShoppingList = false;
            }
        }

        public void EditProductInShoppingList(Product product)
        {
            db.Entry(product).State = EntityState.Modified;
        }
    }
}